<?php

ignore_user_abort(true);
set_time_limit(0);
$repo = realpath('./..');
$branch = 'master';
$output = [];
// update github Repo
$output[] = date('Y-m-d, H:i:s', time()).'<br/>';
$output[] = $repo;
$output[] = '<h3>'.'cd '.$repo.'</h3><pre>';
$output[] = shell_exec('cd '.$repo);
$output[] = '</pre><h3>'.'git stash'.'</h3><pre>';
$output[] = shell_exec('git checkout .');
$output[] = shell_exec('git reset');
$output[] = shell_exec('git clean -fd');
$output[] = shell_exec('rm -Rf var/cache/prod');
$output[] = '</pre><h3>'.'git pull origin '.$_GET['branch'].'</h3><pre>';
$output[] = shell_exec('git pull origin '.$_GET['branch']);
$output[] = shell_exec('cd '.$repo.' && php composer.phar install');
$output[] = '</pre><h3>'.'php bin/console doctrine:migrations:migrate'.'</h3><pre>';
$output[] = shell_exec('cd '.$repo.' && php bin/console doctrine:migrations:migrate');
$output[] = '</pre><h3>'.'php bin/console cache:clear --no-warmup --env=prod'.'</h3><pre>';
$output[] = shell_exec('cd '.$repo.' && php bin/console cache:clear --no-warmup --env=prod');
$output[] = '</pre><h3>'.'php composer.phar install'.'</h3><pre>';
$output[] = shell_exec('cd '.$repo.' && php composer.phar install --no-dev');
$output[] = '</pre>----------------------------';

echo '<pre>';
// affichage du résultat, pour info
echo implode('<br/>', $output);
echo '</pre>';
// redirect output to logs
file_put_contents(rtrim(getcwd(), '/').'/updt-git-log.txt', implode("\n", $output)."\n----------------------------\n");
