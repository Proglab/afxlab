Afxlab
==

Installation
-
```batch
copy .env_ex to .env
php bin/console doctrine:database:create      # création de la DB
composer install                              # IF PROD : composer install --no-dev
php bin/console ckeditor:install              # Install ck editor
php bin/console assets:install --symlink      # Load ck editor depedencies
php bin/console doctrine:migrations:migrate   # création des tables et relations
php bin/console doctrine:fixtures:load        # quelques données dans la DB
```


Comptes Users :
-

| Login 	| Password 	| Role
|--------------------------	|---------	|------------------
| fabrice@proglab.com   	| fabrice 	| ROLE_SUPER_ADMIN
| manu@absolute-fx.com  	| manu   	| ROLE_ADMIN

Tools :
-

### PHP-CS-FIXER :
> Fixe quelques problème de programmation sur les standards habituels (symfony dans ce cas)

#### Download : 
```batch 
composer global require friendsofphp/php-cs-fixer 
```

#### Execution :
```properties 
vendor\bin\php-cs-fixer.bat  fix ./src --rules=@Symfony
```
---
### PHPStan
> Cherche des erreurs dans le PHP

#### Download :
```batch 
composer require --dev phpstan/phpstan
```

#### Execution :
> xxx = un chiffre de 0 à 7. 7 étant une analyse très profonde.
```batch 
vendor\bin\phpstan analyse src  --level=xxx
```
---
### PHP Documentator :
> Génère la documentation

#### Download :
```batch 
wget http://phpdoc.org/phpDocumentor.phar
```

#### Execution :
```batch 
php phpDocumentor.phar -d ./src -t ./docs/PHPDocumentor
```

---
### PHP Metrics :
> Génère des metrics

#### Download :
```batch 
composer require phpmetrics/phpmetrics --dev
```
#### Execution :
```batch 
vendor\bin\phpmetrics --report-html=docs\PHPMetrics .
```

---
### PHP Mess Dectector :
> Détecte les bugs

#### Download :
```batch 
composer require phpmd/phpmd --dev
```
#### Execution :
```batch 
vendor\bin\phpmd src html cleancode,codesize,controversial,design,naming,unusedcode > docs/phpmd.html
```