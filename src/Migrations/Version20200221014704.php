<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200221014704 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE project_technology (project_id INT NOT NULL, technology_id INT NOT NULL, INDEX IDX_ECC5297F166D1F9C (project_id), INDEX IDX_ECC5297F4235D463 (technology_id), PRIMARY KEY(project_id, technology_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE project_user_partner (project_id INT NOT NULL, user_partner_id INT NOT NULL, INDEX IDX_4381C47F166D1F9C (project_id), INDEX IDX_4381C47F34F43E12 (user_partner_id), PRIMARY KEY(project_id, user_partner_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE project_user_client (project_id INT NOT NULL, user_client_id INT NOT NULL, INDEX IDX_6AD9E893166D1F9C (project_id), INDEX IDX_6AD9E893190BE4C5 (user_client_id), PRIMARY KEY(project_id, user_client_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE project_technology ADD CONSTRAINT FK_ECC5297F166D1F9C FOREIGN KEY (project_id) REFERENCES project (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE project_technology ADD CONSTRAINT FK_ECC5297F4235D463 FOREIGN KEY (technology_id) REFERENCES technology (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE project_user_partner ADD CONSTRAINT FK_4381C47F166D1F9C FOREIGN KEY (project_id) REFERENCES project (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE project_user_partner ADD CONSTRAINT FK_4381C47F34F43E12 FOREIGN KEY (user_partner_id) REFERENCES user (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE project_user_client ADD CONSTRAINT FK_6AD9E893166D1F9C FOREIGN KEY (project_id) REFERENCES project (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE project_user_client ADD CONSTRAINT FK_6AD9E893190BE4C5 FOREIGN KEY (user_client_id) REFERENCES user (id) ON DELETE CASCADE');
        $this->addSql('DROP TABLE technology_project');
        $this->addSql('DROP TABLE user_client_project');
        $this->addSql('DROP TABLE user_partner_project');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE technology_project (technology_id INT NOT NULL, project_id INT NOT NULL, INDEX IDX_6EFD95584235D463 (technology_id), INDEX IDX_6EFD9558166D1F9C (project_id), PRIMARY KEY(technology_id, project_id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('CREATE TABLE user_client_project (user_client_id INT NOT NULL, project_id INT NOT NULL, INDEX IDX_160B4BCA190BE4C5 (user_client_id), INDEX IDX_160B4BCA166D1F9C (project_id), PRIMARY KEY(user_client_id, project_id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('CREATE TABLE user_partner_project (user_partner_id INT NOT NULL, project_id INT NOT NULL, INDEX IDX_FA85DF7C34F43E12 (user_partner_id), INDEX IDX_FA85DF7C166D1F9C (project_id), PRIMARY KEY(user_partner_id, project_id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('ALTER TABLE technology_project ADD CONSTRAINT FK_6EFD9558166D1F9C FOREIGN KEY (project_id) REFERENCES project (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE technology_project ADD CONSTRAINT FK_6EFD95584235D463 FOREIGN KEY (technology_id) REFERENCES technology (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE user_client_project ADD CONSTRAINT FK_160B4BCA166D1F9C FOREIGN KEY (project_id) REFERENCES project (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE user_client_project ADD CONSTRAINT FK_160B4BCA190BE4C5 FOREIGN KEY (user_client_id) REFERENCES user (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE user_partner_project ADD CONSTRAINT FK_FA85DF7C166D1F9C FOREIGN KEY (project_id) REFERENCES project (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE user_partner_project ADD CONSTRAINT FK_FA85DF7C34F43E12 FOREIGN KEY (user_partner_id) REFERENCES user (id) ON DELETE CASCADE');
        $this->addSql('DROP TABLE project_technology');
        $this->addSql('DROP TABLE project_user_partner');
        $this->addSql('DROP TABLE project_user_client');
    }
}
