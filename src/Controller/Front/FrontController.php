<?php

namespace App\Controller\Front;

use App\Service\CategoryService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class FrontController extends AbstractController
{
    protected $categoryService;

    public function __construct(CategoryService $categoryService)
    {
        $this->categoryService = $categoryService;
    }

    /**
     * @Route("/", name="homepage")
     */
    public function HomepageAction(Request $request)
    {
        return $this->render('homepage.html.twig');
    }

    /**
     * @Route("/category/{title}", name="webdev")
     */
    public function CategoryAction(Request $request, $title)
    {
        $category = $this->categoryService->getCategory('Website');

        return $this->render('web-dev.html.twig', ['category' => $category]);
    }
}
