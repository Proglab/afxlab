<?php

namespace App\Controller\Admin;

use App\Entity\User;
use Exception;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

class SecurityController extends AbstractController
{
    /**
     * @Route("/admin/login", name="login_admin")
     */
    public function login(AuthenticationUtils $authenticationUtils)
    {
        // if ($this->getUser()) {
        //     return $this->redirectToRoute('target_path');
        // }

        // get the login error if there is one
        $error = $authenticationUtils->getLastAuthenticationError();
        // last username entered by the user
        $lastUsername = $authenticationUtils->getLastUsername();

        return $this->render('/security/login.html.twig', [
            // parameters usually defined in Symfony login forms
            'error' => $error,
            'last_username' => $lastUsername,

            // OPTIONAL parameters to customize the login form:

            // the string used to generate the CSRF token. If you don't define
            // this parameter, the login form won't include a CSRF token
            'csrf_token_intention' => 'authenticate',
            // the URL users are redirected to after the login (default: path('easyadmin'))
            'target_path' => $this->generateUrl('easyadmin'),
            // the label displayed for the username form field (the |trans filter is applied to it)
            'username_label' => 'security.login.email',
            // the label displayed for the password form field (the |trans filter is applied to it)
            'password_label' => 'security.login.password',
            // the label displayed for the Sign In form button (the |trans filter is applied to it)
            'sign_in_label' => 'security.login.submit',
        ]);
    }

    /**
     * @Route("/admin/logout", name="logout")
     */
    public function logout()
    {
        throw new Exception('This method can be blank - it will be intercepted by the logout key on your firewall');
    }

    /**
     * Switch user.
     *
     * @Route("/admin/switch_user/{id}", name="switch_user", requirements={"_locale": "fr|nl"})
     * @Security("is_granted('ROLE_ADMIN')")
     */
    public function switchUserAction(Request $request, User $user): RedirectResponse
    {
        return $this->redirectToRoute('admin_dashboard', [
            '_locale' => $request->getLocale(),
            '_switch_user' => $user->getUsername(),
        ]);
    }
}
