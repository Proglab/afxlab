<?php

namespace App\Service;

use App\Repository\CategoryRepository;

class CategoryService
{
    protected $categoryRepository;

    public function __construct(CategoryRepository $categoryRepository)
    {
        $this->categoryRepository = $categoryRepository;
    }

    public function getCategory($title)
    {
        return $this->categoryRepository->findOneBy(['title' => $title]);
    }
}
