<?php

namespace App\DataFixtures;

use App\Entity\Technology;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

/**
 * Class UsersFixtures.
 */
class TechnologyFixtures extends Fixture
{
    public const TECHNOLOGY = 'Technology';

    public function load(ObjectManager $manager)
    {
        $items = $this->getDatas();

        foreach ($items as $item) {
            $entity = new Technology();
            $entity->setTitle($item['title']);
            $manager->persist($entity);
            $this->addReference(self::TECHNOLOGY.'_'.$item['title'], $entity);
        }

        $manager->flush();
    }

    protected function getDatas()
    {
        return [
            [
                'title' => 'PHP',
            ],
            [
                'title' => 'Symfony',
            ],
            [
                'title' => 'MySQL',
            ],
            [
                'title' => 'Docker',
            ],
            [
                'title' => 'HTML',
            ],
            [
                'title' => 'CSS',
            ],
            [
                'title' => 'JAvascript',
            ],
            [
                'title' => 'Node.js',
            ],
            [
                'title' => 'Three.js',
            ],
            [
                'title' => 'Vue.js',
            ],
            [
                'title' => 'Electron',
            ],
            [
                'title' => 'Python',
            ],
        ];
    }
}
