<?php

namespace App\DataFixtures;

use App\Entity\Category;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker;

/**
 * Class UsersFixtures.
 */
class CategoryFixtures extends Fixture
{
    public const CATEGORY_REFERENCE = 'category';

    public function load(ObjectManager $manager)
    {
        $items = $this->getDatas();

        $faker_en = Faker\Factory::create('en_GB');

        foreach ($items as $item) {
            $entity = new Category();
            $entity->setTitle($item['title']);
            $entity->setSlug($item['slug']);
            $entity->setDescription($faker_en->realText(rand(150, 300), 3));
            $manager->persist($entity);
            $this->addReference(self::CATEGORY_REFERENCE.'_'.str_replace(' ', '-', $item['title']), $entity);
        }

        $manager->flush();
    }

    protected function getDatas()
    {
        return [
            [
                'title' => 'Website',
                'slug' => 'web-dev',
            ],
            [
                'title' => 'ERP',
                'slug' => 'erp',
            ],
            [
                'title' => 'Software',
                'slug' => 'software',
            ],
            [
                'title' => 'Web App',
                'slug' => 'web-app',
            ],
            [
                'title' => '3D',
                'slug' => '3d',
            ],
            [
                'title' => 'Video Game',
                'slug' => 'video-game',
            ],
            [
                'title' => 'Video',
                'slug' => 'video',
            ],
            [
                'title' => 'Drone',
                'slug' => 'drone',
            ],
            [
                'title' => 'Photogrammetry',
                'slug' => 'photogrammetry',
            ],
            [
                'title' => 'IOT',
                'slug' => 'iot',
            ],
        ];
    }
}
