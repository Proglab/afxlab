<?php

namespace App\DataFixtures;

use App\Entity\Company;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

/**
 * Class UsersFixtures.
 */
class CompanyFixtures extends Fixture
{
    public const COMPANY_REFERENCE = 'company';

    protected $manager;

    public function load(ObjectManager $manager)
    {
        $this->manager = $manager;

        $items = $this->getDatas();
        foreach ($items as $item) {
            $parent = $this->getClient($item);
        }
        $this->manager->flush();
    }

    protected function getClient(array $item)
    {
        $parent = new Company();
        $parent->setTitle($item['title']);
        if (isset($item['client'])) {
            foreach ($item['client'] as $client) {
                $clientEntity = $this->getClient($client);
                $parent->addChild($clientEntity);
            }
        }
        $this->manager->persist($parent);
        $this->addReference(self::COMPANY_REFERENCE.'_'.str_replace(' ', '-', $item['title']), $parent);

        return $parent;
    }

    protected function getDatas()
    {
        return
        [
            ['title' => 'Proglab'],
            ['title' => 'Absolute FX'],
            [
                'title' => 'Wabu',
                'client' => [
                    ['title' => 'European community'],
                    ['title' => 'TEDx'],
                    ['title' => 'FIEC'],
                    ['title' => 'Di Philippo'],
                ],
            ],
            ['title' => 'ITM Technology'],
            ['title' => 'Imoges'],
            ['title' => 'Tilt Factory'],
            ['title' => 'VieTamine'],
            ['title' => 'Deny Cargo'],
            [
                'title' => 'MarsMoore',
                'client' => [
                    ['title' => 'Azza'],
                    ['title' => 'Servilog'],
                    ['title' => 'Ozze'],
                    ['title' => 'Activa'],
                    ['title' => 'MémoArt'],
                ],
            ],
            ['title' => 'Taxi Carolo'],
            [
                'title' => 'Tatoo',
                'client' => [
                    [
                        'title' => 'TF1',
                        'client' => [
                            ['title' => 'Star Academy'],
                            ['title' => 'Secret Story'],
                        ],
                    ],
                    [
                        'title' => 'France 2',
                        'client' => [
                            ['title' => 'Plus belle la vie'],
                        ],
                    ],
                    [
                        'title' => 'VTM',
                        'client' => [
                            ['title' => 'The block'],
                        ],
                    ],
                ],
            ],
        ];
    }
}
