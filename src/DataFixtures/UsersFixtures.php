<?php

namespace App\DataFixtures;

use App\Entity\UserClient;
use App\Entity\UserPartner;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

/**
 * Class UsersFixtures.
 */
class UsersFixtures extends Fixture implements DependentFixtureInterface
{
    public const USERS_REFERENCE = 'users';

    protected $passwordEncoder;

    public function __construct(UserPasswordEncoderInterface $passwordEncoder)
    {
        $this->passwordEncoder = $passwordEncoder;
    }

    public function load(ObjectManager $manager)
    {
        $items = $this->getDatasClients();

        foreach ($items as $item) {
            $entity = new UserClient();
            $entity->setPlainPassword($item['password'], $this->passwordEncoder);
            $entity->setEmail($item['email']);
            $entity->setRoles($item['roles']);
            $entity->setLastname($item['firstmane']);
            $entity->setFirstname($item['lastmane']);
            $entity->setCompany($item['company']);
            $manager->persist($entity);
            $this->addReference(self::USERS_REFERENCE.'_'.str_replace(' ', '-', $item['firstmane']).'-'.
                str_replace(' ', '-', $item['lastmane']), $entity);
        }

        $manager->flush();

        $items = $this->getDatasPartners();

        foreach ($items as $item) {
            $entity = new UserPartner();
            $entity->setPlainPassword($item['password'], $this->passwordEncoder);
            $entity->setEmail($item['email']);
            $entity->setRoles($item['roles']);
            $entity->setLastname($item['firstmane']);
            $entity->setFirstname($item['lastmane']);
            $entity->setCompany($item['company']);
            $manager->persist($entity);
            $this->addReference(self::USERS_REFERENCE.'_'.str_replace(' ', '-', $item['firstmane']).'-'.
                str_replace(' ', '-', $item['lastmane']), $entity);
        }

        $manager->flush();
    }

    protected function getDatasPartners()
    {
        return [
            [
                'email' => 'fabrice@proglab.com',
                'password' => 'fabrice',
                'roles' => ['ROLE_SUPER_ADMIN'],
                'firstmane' => 'Fabrice',
                'lastmane' => 'Gyre',
                'company' => $this->getReference(CompanyFixtures::COMPANY_REFERENCE.'_Proglab'),
            ],
            [
                'email' => 'manu@absolute-fx.com',
                'password' => 'manu',
                'roles' => ['ROLE_ADMIN'],
                'firstmane' => 'Manu',
                'lastmane' => 'Pain',
                'company' => $this->getReference(CompanyFixtures::COMPANY_REFERENCE.'_Absolute-FX'),
            ],
        ];
    }

    protected function getDatasClients()
    {
        return [
            [
                'email' => 'mvandermeiren@itm-technologies.com',
                'password' => 'matthieu',
                'roles' => ['ROLE_CLIENT'],
                'firstmane' => 'Matthieu',
                'lastmane' => 'Van Der Meiren',
                'company' => $this->getReference(CompanyFixtures::COMPANY_REFERENCE.'_ITM-Technology'),
            ],
        ];
    }

    public function getDependencies()
    {
        return [
            CompanyFixtures::class,
        ];
    }
}
