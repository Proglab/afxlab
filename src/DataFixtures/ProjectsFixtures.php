<?php

namespace App\DataFixtures;

use App\Entity\Project;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;

/**
 * Class UsersFixtures.
 */
class ProjectsFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $items = $this->getDatas();

        foreach ($items as $item) {
            $entity = new Project();
            $entity->setTitle($item['title']);
            $entity->setDescription($item['description']);
            $entity->setPrestatationDt($item['prestationDt']);
            foreach ($item['clients'] as $client) {
                $entity->addClient($client);
            }
            foreach ($item['categories'] as $category) {
                $entity->addCategory($category);
            }
            foreach ($item['technologies'] as $technology) {
                $entity->addTechnology($technology);
            }
            foreach ($item['partners'] as $partner) {
                $entity->addPartner($partner);
            }
            $manager->persist($entity);
        }

        $manager->flush();
    }

    protected function getDatas()
    {
        return [
            [
                'title' => 'WOM',
                'description' => 'Work Order Managment',
                'prestationDt' => \DateTime::createFromFormat('Y/m/d', '2013/05/19'),
                'clients' => [
                    $this->getReference(UsersFixtures::USERS_REFERENCE.'_Matthieu-Van-Der-Meiren'),
                ],
                'categories' => [
                    $this->getReference(CategoryFixtures::CATEGORY_REFERENCE.'_ERP'),
                ],
                'partners' => [
                    $this->getReference(UsersFixtures::USERS_REFERENCE.'_Fabrice-Gyre'),
                    $this->getReference(UsersFixtures::USERS_REFERENCE.'_Manu-Pain'),
                ],
                'technologies' => [
                    $this->getReference(TechnologyFixtures::TECHNOLOGY.'_PHP'),
                    $this->getReference(TechnologyFixtures::TECHNOLOGY.'_MySQL'),
                ],
            ],
        ];
    }

    public function getDependencies()
    {
        return [
            CategoryFixtures::class,
            UsersFixtures::class,
            TechnologyFixtures::class,
        ];
    }
}
